package com.example.calculator

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.calculator.ui.theme.CalculatorTheme

class MainActivity : ComponentActivity() {
    private val mainViewModel by viewModels<Equation>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CalculatorTheme {
                val result: Double by mainViewModel.result.collectAsState()
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    CalculatorDisplay(mainViewModel, result)
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CalculatorDisplay(eq: Equation, result: Double) {
    var text by remember { mutableStateOf("") }
    var text2 by remember { mutableStateOf("") }
    var expanded by remember { mutableStateOf(false) }
    val items = listOf("+", "-", "*", "/", "%", "^")
    var selectedIndex by remember { mutableStateOf(0) }
    Column() {
        Row(modifier = Modifier.fillMaxWidth()) {
            Column(modifier = Modifier.weight(1F)) {
                TextField(
                    value = text,
                    onValueChange = { newText ->
                        text = newText.filter { c -> c.isDigit() }},
                    label = { Text("Field 1") },
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
                )
            }
            Column(modifier = Modifier.weight(0.5f)) {
                Text(items[selectedIndex],modifier = Modifier
                    .fillMaxWidth()
                    .clickable(
                        onClick = { expanded = true })
                    .background(Color.Cyan))
                DropdownMenu(
                    expanded = expanded,
                    onDismissRequest = { expanded = false },
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(
                            Color.Cyan
                        )
                ) {
                    items.forEachIndexed { index, s ->
                        DropdownMenuItem(onClick = {
                            selectedIndex = index
                            expanded = false
                        },
                            text = {Text(text = s)} )
                    }
                }
            }
            Column(modifier = Modifier.weight(1F)) {
                TextField(
                    value = text2,
                    onValueChange = { newText ->
                        text2 = newText.filter { c -> c.isDigit() }},
                    label = { Text("Field 2") },
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number))
            }

        }
        Row(modifier = Modifier
            .fillMaxWidth(),
            horizontalArrangement = Arrangement.Center) {
            Box(modifier = Modifier
                .clip(CircleShape)
                .background(Color.Magenta))
            {
                Text(text = "Calculate", modifier = Modifier.clickable(
                    onClick = {eq.parseEquation(text.toInt(), text2.toInt(), items[selectedIndex])}))
            }
        }
        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
            Text(text = result.toString(), modifier = Modifier.background(Color.Green))

        }
    }


    
}

@Preview(showBackground = true)
@Composable
fun CalculatorPreview() {
    CalculatorTheme {
        CalculatorDisplay(Equation(), 0.0)
    }
}