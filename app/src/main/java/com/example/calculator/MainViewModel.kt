package com.example.calculator

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class Equation: ViewModel() {
    private val _result = MutableStateFlow(0.0)
    val result: StateFlow<Double> get() = _result
    fun parseEquation(firstValue: Int, secondValue: Int, operation: String) {
         when (operation) {
            "+" -> _result.value = add(firstValue, secondValue)
            "-" -> _result.value =subtract(firstValue, secondValue)
            "*" -> _result.value =multiply(firstValue, secondValue)
            "/" -> _result.value =divide(firstValue, secondValue)
            "%" -> _result.value =mod(firstValue, secondValue)
            "^" -> _result.value =exp(firstValue, secondValue)
            else ->  _result.value = 0.0
        }
    }

    private fun exp(base: Int, exp: Int): Double {
        when (exp) {
            0 -> return base.toDouble()
            else -> return base * exp(base, exp-1)
        }
    }

    private fun mod( firstValue: Int,  secondValue: Int): Double {
        return (firstValue % secondValue).toDouble()
    }

    private fun divide( firstValue: Int,  secondValue: Int): Double {
        return firstValue.toDouble() / secondValue
    }

    private fun multiply( firstValue: Int,  secondValue: Int): Double {
        return (firstValue * secondValue).toDouble()
    }

    private fun subtract( firstValue: Int,  secondValue: Int): Double {
        return (firstValue - secondValue).toDouble()
    }

    private fun add( firstValue: Int,  secondValue: Int): Double {
        return (firstValue + secondValue).toDouble()
    }
}